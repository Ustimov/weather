package org.ustimov.weather.ustimovweather;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;

import ru.mail.weather.lib.City;
import ru.mail.weather.lib.Weather;
import ru.mail.weather.lib.WeatherStorage;
import ru.mail.weather.lib.WeatherUtils;

public class WeatherService extends IntentService {

    public final static String NOTFICATION = "UPDATE_UI";

    public WeatherService() {
        super("WeatherService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            City city = WeatherStorage.getInstance(this).getCurrentCity();
            Weather w = WeatherUtils.getInstance().loadWeather(city);
            WeatherStorage.getInstance(this).saveWeather(city, w);
            Intent i = new Intent(NOTFICATION);
            LocalBroadcastManager.getInstance(this).sendBroadcast(i);
        }
        catch (Exception e) {
            Log.d("WEATHERSERVICE", "UPDATE EXCEPTION");
        }

    }
}
