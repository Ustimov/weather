package org.ustimov.weather.ustimovweather;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ru.mail.weather.lib.City;
import ru.mail.weather.lib.Weather;
import ru.mail.weather.lib.WeatherStorage;
import ru.mail.weather.lib.WeatherUtils;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "TAG";

    private TextView showText;
    private TextView tempText;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            City city = WeatherStorage.getInstance(MainActivity.this).getCurrentCity();
            Weather weather = WeatherStorage.getInstance(MainActivity.this).getLastSavedWeather(city);
            tempText.setText(Integer.toString(weather.getTemperature()));
            showText.setText(weather.getDescription());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showText = (TextView) findViewById(R.id.descriptionText);
        tempText = (TextView) findViewById(R.id.temperatureText);

        Button selectCityButton = (Button)findViewById(R.id.selectCity);
        selectCityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCityActivity();
            }
        });

        City city = WeatherStorage.getInstance(this).getCurrentCity();
        selectCityButton.setText(city.toString());

        Button enableBackgroundUpdateButton = (Button) findViewById(R.id.enableBackgroundUpdate);
        enableBackgroundUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, WeatherService.class);
                WeatherUtils.getInstance().schedule(MainActivity.this, intent);
                Log.d(TAG, "BACKGROUND START");
            }
        });

        Button disableBackgroundUpdateButton = (Button) findViewById(R.id.disableBackgroundUpdate);
        disableBackgroundUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, WeatherService.class);
                WeatherUtils.getInstance().unschedule(MainActivity.this, intent);
                Log.d(TAG, "BACKGROUND STOP");
            }
        });
    }

    private void startCityActivity() {
        Intent intent = new Intent(this, CityActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        Button selectCityButton = (Button)findViewById(R.id.selectCity);
        City city = WeatherStorage.getInstance(this).getCurrentCity();
        selectCityButton.setText(city.toString());
        Weather weather = WeatherStorage.getInstance(MainActivity.this).getLastSavedWeather(city);
        if (weather != null)
        {
            tempText.setText(Integer.toString(weather.getTemperature()));
            showText.setText(weather.getDescription());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(WeatherService.NOTFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }
}
