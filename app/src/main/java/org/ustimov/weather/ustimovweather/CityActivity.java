package org.ustimov.weather.ustimovweather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;

import ru.mail.weather.lib.City;
import ru.mail.weather.lib.WeatherStorage;

public class CityActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        Button selectViceCity = (Button) findViewById(R.id.selectViceCity);
        selectViceCity.setOnClickListener(this);
        Button selectRaccoonCity = (Button) findViewById(R.id.selectRaccoonCity);
        selectRaccoonCity.setOnClickListener(this);
        Button selectSpringfield = (Button) findViewById(R.id.selectSpringfield);
        selectSpringfield.setOnClickListener(this);
        Button selectSilentHill = (Button) findViewById(R.id.selectSilentHill);
        selectSilentHill.setOnClickListener(this);
        Button selectSouthPark = (Button) findViewById(R.id.selectSouthPark);
        selectSouthPark.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Button button = (Button) view;
        City city = City.valueOf(button.getText().toString());
        WeatherStorage.getInstance(this).setCurrentCity(city);
        finish();
    }
}
